﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace BattleshipClient
{
    class Program
    {
        static Dictionary<string, int> LetterMap = new Dictionary<string, int>() { {"A",0},{"B",1},{"C",2},{"D",3},{"E",4},{"F",5},{ "G", 6 },{ "H", 7 },{ "I", 8 },{ "J", 9 }};
        static string serverIP = "localhost";
        static int port = 8080;

        static void Main()
        {
            //string serverIP = "localhost";
            //int port = 8080;
            //char TerminatingChar = (char)4;
            string input;
            int cont = 1;
            while (cont != 0)
            {
                Console.WriteLine(" ------ Enter Command  ------ ");
                Console.WriteLine("### ['test'|'play'|'quit'] ###");
                input = Console.ReadLine();
                switch (input)
                {
                    case "test":
                        Console.WriteLine("\n");
                        TestConnection(serverIP, port);
                        break;
                    case "play":
                        Console.WriteLine("\n");
                        PlayGame(serverIP, port);
                        break;
                    case "quit":
                        //TransmitSignal(serverIP, port, "quit");
                        cont = 0;
                        break;
                    case "guess":
                        Console.WriteLine("\n");
                        MakeGuess();
                        break;
                    default:
                        Console.WriteLine("Invalid Command");
                        break;
                }
                //Console.WriteLine("\n");
            }

        }

        //static void InitMap()
        //{
        //    LetterMap["A"] = 0;
        //    LetterMap["B"] = 1;
        //    LetterMap["C"] = 2;
        //    LetterMap["D"] = 3;
        //    LetterMap["E"] = 4;
        //    LetterMap["F"] = 5;
        //    LetterMap["G"] = 6;
        //    LetterMap["H"] = 7;
        //    LetterMap["I"] = 8;
        //    LetterMap["J"] = 9;
        //}

        static void TestConnection(string IP, int port)
        {
            try
            {
                TcpClient client = new TcpClient(IP, port);
                string Transmission = "Client testing connection, do you read?";
                int byteCount = Encoding.ASCII.GetByteCount(Transmission);
                byte[] byteTransmission = new byte[byteCount];
                Console.WriteLine($"Testing Connection, transmitting {byteCount} bytes to server...");
                byteTransmission = Encoding.ASCII.GetBytes(Transmission );
                NetworkStream stream = client.GetStream();
                stream.Write(byteTransmission, 0, byteTransmission.Length);
                Console.WriteLine("Connection Successful");
                //System.Threading.Thread.Sleep(3000);
                
                //client.Dispose(); //maybe include?
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to Connect:");
                Console.WriteLine(ex.ToString());

            }
            
        }
        static void TransmitSignal(string IP, int port, string signal)
        {
            try
            {
                TcpClient client = new TcpClient(IP, port);
                //string Transmission = signal;
                int byteCount = Encoding.ASCII.GetByteCount(signal);
                byte[] byteTransmission = new byte[byteCount];
                Console.WriteLine("Sending Signal to Server");
                byteTransmission = Encoding.ASCII.GetBytes(signal);
                NetworkStream stream = client.GetStream();
                stream.Write(byteTransmission, 0, byteTransmission.Length);
                Console.WriteLine("Signal Sent Successfully");

                //client.Dispose(); //Maybe include?
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to Connect to Server:");
                Console.WriteLine(ex.ToString());

            }
        }

        

        static void PlayGame(string IP,int port)
        {
            char[,] GameBoard = new char[10, 10];

            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    GameBoard[i, j] = '#';
                }
            }
            GameBoard = GetPositions(GameBoard);
            try
            {
                TcpClient client = new TcpClient(IP, port);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to establish connection with server");
                Console.WriteLine(ex.ToString());
                return;
            }
            Console.WriteLine("Make a guess:");
        }

        static char[,] GetPositions(char[,] board)
        {
            //Submarine = 3 spaces
            //Destroyer = 3 spaces
            //Battleship = 4 spaces
            //Carrier = 5 spaces
            //PT Boat = 2 spaces
            //Board is 10*10
            
            board = PlaceShip(board, 5, "Aircraft Carrier", 'C');
            board = PlaceShip(board, 4, "Battleship", 'B');
            board = PlaceShip(board, 3, "Destroyer", 'D');
            board = PlaceShip(board, 3, "Submarine", 'S');
            board = PlaceShip(board, 2, "Patrol Boat", 'P');
            Console.WriteLine("Great, all of your ships are placed!");
            Console.WriteLine("Here's your Game Board:");
            PrintBoard(board);
            return board;
        }

        static char[,] PlaceShip(char[,] Board, int ShipSize, string ShipType, char Fill)
        {
            int ShipPlaced = 0;
            string input;
            int hx, hy, tx, ty, clear;
            string[] CoordPair, hPair, tPair;
            while (ShipPlaced != 1)
            {
                Console.WriteLine("Here's the Game Board:");
                PrintBoard(Board);
                Console.WriteLine("");
                Console.WriteLine($"Please enter the head and tail positions for your {ShipType} ({ShipSize} spaces long).");
                Console.WriteLine("The format should be like: 'A-1,A-6' or 'B-2,G-2' ");
                input = Console.ReadLine();
                input = input.Trim();
                if ((input.Length >= 7) && (input.Length <= 9)) //checks if input is something like A-10,D-10 or A-2,A-9
                {
                    CoordPair = input.Split(',');

                    if (CoordPair.Length == 2) //make sure there is only one comma separated pair
                    {
                        //Extract head and tail coordinate pairs
                        hPair = CoordPair[0].Split('-');
                        tPair = CoordPair[1].Split('-');
                        try
                        {
                            //NOTE: accessing board is Board[y,x]
                            hx = LetterMap[hPair[0].Trim()];
                            hy = Convert.ToInt32(hPair[1].Trim());
                            tx = LetterMap[tPair[0].Trim()];
                            ty = Convert.ToInt32(tPair[1].Trim());
                            //Console.WriteLine($"You Chose: {hx},{hy},{tx},{ty}");
                            if ((-1 < hx) && (hx < 10) && (-1 < hy) && (hy < 10) && (-1 < tx) && (tx < 10) && (-1 < ty) && (ty < 10))
                            {
                                if ((hx < tx) && (tx - hx == (ShipSize - 1) ) && (hy == ty))
                                {
                                    //parallel to y-axis, head is south
                                    clear = 1;
                                    for (int j = hx; j <= tx; j++)
                                    {
                                        if (Board[hy,j]!='#')
                                        {
                                            clear = 0;
                                            break;
                                        }
                                    }
                                    if (clear == 1)
                                    {
                                        for (int j = hx; j <= tx; j++)
                                        {
                                            Board[hy, j] = Fill;
                                        }
                                        ShipPlaced = 1;
                                    }
                                    else
                                    {
                                        Console.WriteLine("Cannot place here, position obstructed");
                                    }
                                }
                                else if ((hx > tx) && (hx - tx == (ShipSize - 1)) && (hy == ty))
                                {
                                    //parallel to y-axis, head is north
                                    clear = 1;
                                    for (int j = tx; j <= hx; j++)
                                    {
                                        if (Board[hy, j] != '#')
                                        {
                                            clear = 0;
                                            break;
                                        }
                                    }
                                    if (clear == 1)
                                    {
                                        for (int j = tx; j <= hx; j++)
                                        {
                                            Board[hy, j] = Fill;
                                        }
                                        ShipPlaced = 1;
                                    }
                                    else
                                    {
                                        Console.WriteLine("Cannot place here, position obstructed");
                                    }
                                }
                                else if ((hy > ty) && (hy - ty == (ShipSize - 1)) && (hx == tx))
                                {
                                    //parallel to x-axis, head is east
                                    clear = 1;
                                    for (int j = ty; j <= hy; j++)
                                    {
                                        if (Board[j, hx] != '#')
                                        {
                                            clear = 0;
                                            break;
                                        }
                                    }
                                    if (clear == 1)
                                    {
                                        for (int j = ty; j <= hy; j++)
                                        {
                                            Board[j, hx] = Fill;
                                        }
                                        ShipPlaced = 1;
                                    }
                                    else
                                    {
                                        Console.WriteLine("Cannot place here, position obstructed");
                                    }
                                }
                                else if ((hy < ty) && (ty - hy == (ShipSize - 1)) && (hx == tx))
                                {
                                    //parallel to x-axis, head is west
                                    clear = 1;
                                    for (int j = hy; j <= ty; j++)
                                    {
                                        if (Board[j, hx] != '#')
                                        {
                                            clear = 0;
                                            break;
                                        }
                                    }
                                    if (clear == 1)
                                    {
                                        for (int j = hy; j <= ty; j++)
                                        {
                                            Board[j, hx] = Fill;
                                        }
                                        ShipPlaced = 1;
                                    }
                                    else
                                    {
                                        Console.WriteLine("Cannot place here, position obstructed");
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("Incompatible values.");
                                }
                            }
                            else
                            {
                                Console.WriteLine("Incompatible values.");
                            }

                        }
                        catch
                        {
                            Console.WriteLine("Incorrect Format");
                        }
                    }
                }
                Console.WriteLine("");
            }
            return Board;
        }

        static void PrintBoard(char[,] Board)
        {
            
            for (int i = 0; i < 10; i++)
            {
                Console.Write(i);
                Console.Write('|');
                for (int j = 0; j < 10; j++)
                {
                    Console.Write(Board[i, j]);
                    Console.Write(" ");
                }
                
                Console.WriteLine("");
            }
            Console.WriteLine("--- - - - - - - - - -");
            Console.WriteLine("  A B C D E F G H I J");
            Console.WriteLine("");
        }

        static string MakeGuess()
        {
            bool ValidGuess = false;
            string input = "";
            while (!ValidGuess)
            {
                Console.WriteLine("Enter firing coordinates (format: 'C-7')");
                input = Console.ReadLine();
                input = input.Trim().ToUpper();
                try
                {
                    int RowAsciiVal = Convert.ToInt32(input[0]);
                    int Column = (int)Char.GetNumericValue(input[2]);
                    if (input.Length == 3 && RowAsciiVal >= 65 && RowAsciiVal <= 74 && Column >= 0 && Column <= 9 )
                    {
                        ValidGuess = true;
                        Console.WriteLine($"Returning: {input}");
                        return input;
                    }
                    else
                    {
                        Console.WriteLine("Incorrect format, please try again");
                        Console.WriteLine("\n");
                    }
                }
                catch
                {
                    Console.WriteLine("Incorrect format, please try again");
                    Console.WriteLine("\n");
                }
                
            }
            Console.WriteLine($"Returning: {input}");
            return input;
        }
    }

    class GameBoard
    {
        private char[,] _boardPositions;
        private int _width;
        private int _height; 

        public GameBoard()
        {
            _width = 10;
            _height = 10;
            _boardPositions = new char[_width,_height];
            FillBoard('#');
        }

        public GameBoard(char fillChar)
        {
            _width = 10;
            _height = 10;
            _boardPositions = new char[_width, _height];
            FillBoard(fillChar);
        }

        private void FillBoard(char fillCharacter)
        {
            for (int i = 0; i < _height; i++)
            {
                for (int j = 0; j < _width; j++)
                {
                    _boardPositions[j, i] = fillCharacter;
                }
            }
        }

        private void PrintBoard()
        {
            for (int i = 0; i < _height; i++)
            {
                Console.Write(i);
                Console.Write('|');
                for (int j = 0; j < _width; j++)
                {
                    Console.Write(_boardPositions[j, i]);
                    Console.Write(" ");
                }

                Console.WriteLine("");
            }
            if (_width == 10)
            {
                Console.WriteLine("--- - - - - - - - - -");
                Console.WriteLine("  A B C D E F G H I J");
            }
            Console.WriteLine("");
        }
    }

    class PlayerBoard : GameBoard
    {
        
    }
}
