import threading
import random

def __main__():
    import socket
    sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    try:
        sock.bind(('localhost',8080))
    except Exception as e:
        print("Socket Binding Failed:")
        print(e)
        return

    sock.listen(6)
    cont = True
    while cont:
        clientSock, addr = sock.accept()
        threading._start_new_thread(HandleConnection,(clientSock,addr,sock))
        #threading.Thread(target=HandleConnection,args=(clientSock,addr,)).start()
        

def HandleConnection(clientSock, addr,sock):
    chat = True
    while chat:
        print(f"Connection with {addr} established")
        clientMessage = ''
        dec = True
        while dec:
            message = clientSock.recv(8) #receive message in batches of 8 bytes
            if len(message) < 8: 
                dec = False
                clientMessage += message.decode("ASCII")
            else: 
                clientMessage += message.decode("ASCII")
        #clientMessage = getMessage(clientSock) #Apparently this doesnt work...
        #an operation was attempted on something that is not a socket
        if "test" in clientMessage:
            print(f"{clientMessage}")
            clientSock.close()
            print(f"Test Connection with {addr} complete")
            chat = False
        elif "quit" in clientMessage:
            chat = False
            print(f"Closing connection with {addr}")
            clientSock.close()
            print("Quitting...")
            try:
                sock.close()
            except Exception as e:
                print("Error while closing server socket: ")
                print(e)
                print("\n")
        elif "play" in clientMessage:
            play_game()


def getMessage(clientSock):
    clientMessage = ''
    dec = True
    while dec:
        message = clientSock.recv(8) #receive message in batches of 8 bytes
        if len(message) < 8: 
            dec = False
            clientMessage += message.decode("ASCII")
        else: 
            clientMessage += message.decode("ASCII")
    #print(f"Length of received message: {len(clientMessage)}")
    return clientMessage
    
def sendMessage(clientSock, m):
    clientSock.send(b"{}".format(m))

def play_game():
    print("Playing Game")
    # ----Decide on Board Placements ---- #
    ServerBoard = []
    for i in range(10):
        ServerBoard.append(['~','~','~','~','~','~','~','~','~','~'])
    OpenPositions = {
        0 : [0,1,2,3,4,5,6,7,8,9],
        1 : [0,1,2,3,4,5,6,7,8,9],
        2 : [0,1,2,3,4,5,6,7,8,9],
        3 : [0,1,2,3,4,5,6,7,8,9],
        4 : [0,1,2,3,4,5,6,7,8,9],
        5 : [0,1,2,3,4,5,6,7,8,9],
        6 : [0,1,2,3,4,5,6,7,8,9],
        7 : [0,1,2,3,4,5,6,7,8,9],
        8 : [0,1,2,3,4,5,6,7,8,9],
        9 : [0,1,2,3,4,5,6,7,8,9]
    }
    ServerBoard,OpenPositions = PlaceShip(ServerBoard, 5, OpenPositions, "C") #Aircraft Carrier
    ServerBoard,OpenPositions = PlaceShip(ServerBoard, 4, OpenPositions, "B") #Battleship
    ServerBoard,OpenPositions = PlaceShip(ServerBoard, 3, OpenPositions, "D") #Destroyer
    ServerBoard,OpenPositions = PlaceShip(ServerBoard, 3, OpenPositions, "S") #Submarine
    ServerBoard,OpenPositions = PlaceShip(ServerBoard, 2, OpenPositions, "P") #Patrol Boat
    PrintBoard(ServerBoard)
    # ---- Board Placements Established ---- #
    
    return

#This function is intended to be "destructive" for both the 2D array board and OpenPositions dictionary
#So it shouldn't have to return anything
def PlaceShip(board, shipLength, OpenPositions, Filler):
    picked = False
    #print("Placing: ", Filler)
    while not picked:
        start_y = random.randrange(10)
        if len(OpenPositions[start_y]) >= 1:
            start_x = random.choice(OpenPositions[start_y])
            directions = [1,2,3,4]
            #1 will be up (in dict)
            #2 will be right
            #3 will be down
            #4 will be left

            #Rule out impossible orientations:
            if start_y < shipLength:
                directions.remove(1)
            if (10 - start_y) < shipLength:
                directions.remove(3)
            if start_x < shipLength:
                directions.remove(4)
            if (10 - start_x) < shipLength:
                directions.remove(2)
            
            directionPicked = False
            hasDirection = False
            #If these two booleans are different by the end of the loop, we have to choose 
            #a different starting position / there are no possible orientations of the ship
            #from this spot
            while not directionPicked:
                if len(directions) >= 1:
                    #Must have at least one possible orientation to choose from
                    d = random.choice(directions)
                    DirectionWorks = True
                    if d==1:
                        #go up
                        #print("direction is up")
                        #for g in range(shipLength):
                        g = 0
                        while (g<shipLength) and DirectionWorks:
                            if start_x not in OpenPositions[start_y - g]:
                                DirectionWorks = False
                                directions.remove(1)
                            #Debug helper:
                            # else:
                            #     print(start_y - g, " contains", start_x)
                            #     print(OpenPositions[start_y - g])
                            g+=1
                            
                        if DirectionWorks:
                            #print("Found a workable Direction!")
                            for h in range(shipLength):
                                try:
                                    OpenPositions[start_y - h].remove(start_x)
                                except:
                                    print("Error removing from OpenPositions when moving up!")
                                board[start_x][start_y - h] = Filler
                    elif d==2:
                        #go right
                        #print("direction is right")
                        #for g in range(shipLength):
                        g = 0
                        while (g<shipLength) and DirectionWorks:
                            if (start_x+g) not in OpenPositions[start_y]:
                                DirectionWorks = False
                                directions.remove(2)
                            #Debug helper:
                            # else:
                            #     print(start_x + g, "is in OpenPositions[start_y]!")
                            #     print(OpenPositions[start_y])
                            g+=1
                            
                        if DirectionWorks:
                            #print("Found a workable Direction!")
                            for h in range(shipLength):
                                try:
                                    OpenPositions[start_y].remove(start_x + h)
                                except:
                                    print("Error removing from OpenPositions when moving right!")
                                board[start_x + h][start_y] = Filler
                            
                    elif d==3:
                        #go down
                        #print("direction is down")
                        #for g in range(shipLength):
                        g = 0
                        while (g<shipLength) and DirectionWorks:
                            if start_x not in OpenPositions[start_y + g]:
                                DirectionWorks = False
                                directions.remove(3)
                            #Debug helper:
                            # else: #Delete this...
                            #     print(start_y + g, "contains", start_x)
                            #     print(OpenPositions[start_y + g])
                            g+=1
                            
                        if DirectionWorks:
                            #print("Found a workable Direction!")
                            for h in range(shipLength):
                                try:
                                    OpenPositions[start_y + h].remove(start_x)
                                except:
                                    print("Error removing from OpenPositions when moving down!")
                                board[start_x][start_y + h] = Filler
                            
                    elif d==4:
                        #go left
                        #print("direction is left")
                        g = 0
                        while (g<shipLength) and DirectionWorks:
                            if (start_x - g) not in OpenPositions[start_y]:
                                DirectionWorks = False
                                directions.remove(4)
                            #Debug helper:
                            # else: #Delete this...
                            #     print(start_x - g, "is in OpenPositions[start_y]!")
                            #     print(OpenPositions[start_y])
                            g+=1
                            
                        if DirectionWorks:
                            #print("Found a workable Direction!")
                            for h in range(shipLength):
                                try:
                                    OpenPositions[start_y].remove(start_x - h)
                                except:
                                    print("Error removing from OpenPositions when moving left!")

                                board[start_x - h][start_y] = Filler
                            
                    if DirectionWorks:
                        directionPicked = True
                        hasDirection = True
                        
                else:
                    directionPicked = True
                    hasDirection = False

            #print("Escaped Direction Loop!")
            if hasDirection and directionPicked:
                picked = True
    return (board,OpenPositions)


def PrintBoard(board):
    for y in board:
        for x in y:
            print(x," ", end="")
        print("")


#play_game()
__main__()